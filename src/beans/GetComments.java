package beans;

import java.io.Serializable;
import java.util.Date;

public class GetComments implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String text;
	private String comment_name;
	private String comment_loginid;
	private int message;
	private Date created_date;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getComment_name() {
		return comment_name;
	}

	public void setComment_name(String comment_name) {
		this.comment_name = comment_name;
	}

	public String getComment_loginid() {
		return comment_loginid;
	}

	public void setComment_loginid(String comment_loginid) {
		this.comment_loginid = comment_loginid;
	}

	public int getMessage() {
		return message;
	}

	public void setMessage(int message) {
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

}