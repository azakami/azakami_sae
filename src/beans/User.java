package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String login_id;
	private String password;
	private String name;
	private int branch;
	private String branchName;
	private int position;
	private String positionName;
	private byte stop;
	private Timestamp created_date;
	private Timestamp updated_date;

	public User() {
	};

	public User(int id, String login_id, String name, String branchName, String positionName, byte stop, Timestamp created_date,
			Timestamp updated_date) {
		this.id = id;
		this.login_id = login_id;
		this.name = name;
		this.branchName = branchName;
		this.positionName = positionName;
		this.stop = stop;
		this.created_date = created_date;
		this.updated_date = updated_date;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch(int branch) {
		this.branch = branch;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public byte getStop() {
		return stop;
	}

	public void setStop(byte stop) {
		this.stop = stop;
	}

	public Timestamp getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Timestamp created_Date) {
		this.created_date = created_Date;
	}

	public Timestamp getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Timestamp updated_Date) {
		this.updated_date = updated_Date;
	}
}
