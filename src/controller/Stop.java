package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class Stop
 */
@WebServlet("/Stop")
public class Stop extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Stop() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		if (session.getAttribute("loginUser") == null) {
			response.sendRedirect("login");
			return;
		}

		User stop = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		byte stopNumber = stop.getStop();

		if (stopNumber == 0) {
			stopNumber++;
			stop.setStop(stopNumber);
		} else {
			stopNumber--;
			stop.setStop(stopNumber);
		}
		new UserService().stop(stop);

		response.sendRedirect("UserManagement");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
