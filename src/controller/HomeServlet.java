package controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comment;
import beans.GetComments;
import beans.Message;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

/**
 * Servlet implementation class gohome
 */
@WebServlet("/index.jsp")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		UserMessage userMessage = new UserMessage();
		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		if (session.getAttribute("loginUser") == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
			return;
		}


		//投稿全出力
		if ((request.getParameter("category") == null || request.getParameter("category") == "")
				&& (request.getParameter("start") == null || request.getParameter("start") == "")
				&& (request.getParameter("end") == null || request.getParameter("end") == "")) {
			List<UserMessage> ret = new MessageService().getMessage();
			List<GetComments> comments = new CommentService().getComments();
			request.setAttribute("ret", ret);
			request.setAttribute("comments", comments);
			request.getRequestDispatcher("WEB-INF/jsp/home.jsp").forward(request, response);
			return;
		}

		userMessage.setCategory(request.getParameter("category"));

		String startDate = request.getParameter("start");
		if (startDate == "") {
			startDate = "2019-01-01";
		}
		Timestamp start = null;
		try {
			start = new Timestamp(
					new SimpleDateFormat("yyyy-MM-dd").parse(startDate).getTime());
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		String endDate = request.getParameter("end");
		if (endDate == "") {
			endDate = "3000-01-01";
		}
		Timestamp end = null;
		try {
			end = new Timestamp(
					new SimpleDateFormat("yyyy-MM-dd").parse(endDate).getTime());
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		userMessage.setCreated_date(start);
		userMessage.setEndCreated_date(end);

		//複合検索すべて
		if ((request.getParameter("category") != "")
				&& (request.getParameter("start") != "" || request.getParameter("end") != "")) {

			List<UserMessage> categoryPeriod = new MessageService().getCategoryPeriod(userMessage);
			request.setAttribute("ret", categoryPeriod);

			List<GetComments> comments = new CommentService().getComments();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("WEB-INF/jsp/home.jsp").forward(request, response);
			return;
		}

		//カテゴリー指定のみ
		if ((request.getParameter("category") != "") && (request.getParameter("start") == "")
				&& (request.getParameter("end") == "")) {
			userMessage.setCategory(request.getParameter("category"));
			List<UserMessage> category = new MessageService().getCategory(userMessage);
			request.setAttribute("ret", category);

			List<GetComments> comments = new CommentService().getComments();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("WEB-INF/jsp/home.jsp").forward(request, response);
			return;

		}

		//投稿日期間全指定のみ
		if ((request.getParameter("start") != "" || request.getParameter("end") != "")
				&& (request.getParameter("category") == "")) {

			List<UserMessage> period = new MessageService().getPeriod(userMessage);
			request.setAttribute("ret", period);
			List<GetComments> comments = new CommentService().getComments();
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("WEB-INF/jsp/home.jsp").forward(request, response);
			return;
		}

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		if (request.getParameter("message_id") != null) {
			Message message = new Message();
			message.setId(Integer.parseInt(request.getParameter("message_id")));
			new MessageService().delete(message);
		}
		if (request.getParameter("comment_id") != null) {
			Comment comment = new Comment();
			comment.setId(Integer.parseInt(request.getParameter("comment_id")));
			new CommentService().delete(comment);
		}

		response.sendRedirect("./");

	}
}
