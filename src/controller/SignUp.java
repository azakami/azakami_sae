package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class SignUp
 */
@WebServlet("/SignUp")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();
		if (user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
			return;
		} else if (user.getPosition() != 1) {
			messages.add("アクセス権限が付与されていません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		}

		request.getRequestDispatcher("WEB-INF/jsp/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		//セッションスコープを取得

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			String login_id = request.getParameter("login_id");
			String password = request.getParameter("password");
			String name = request.getParameter("name");
			int branch = Integer.parseInt(request.getParameter("branch"));
			int position = Integer.parseInt(request.getParameter("position"));

			User user = new User();
			user.setLogin_id(login_id);
			user.setPassword(password);
			user.setName(name);
			user.setBranch(branch);
			user.setPosition(position);

			new UserService().register(user);
			//ユーザー管理画面に遷移
			response.sendRedirect("UserManagement");
		} else {
			//スコープにインスタンスを保存("属性名",インスタンス)
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("SignUp");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String chekePassword = request.getParameter("chekePassword");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position  = request.getParameter("position");

		if (login_id.length() < 6 || login_id.length() > 20 || !login_id.matches("^[0-9a-zA-Z]+$")) {
			messages.add("ログインID(半角英数字で6文字以上20文字以下)を入力してください。");
		}

		if (password.length() < 6 || password.length() > 20
				|| !password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")) {
			messages.add("パスワード(記号を含む全ての半角文字で6文字以上20文字以下)を入力してください。");
		}

		if (!password.equals(chekePassword)) {
			messages.add("登録用パスワードと確認用パスワードの値が相違しています。");
		}

		if (StringUtils.isEmpty(name) == true || name.length() > 10) {
			messages.add("名前(10文字以下)を入力してください。");
		}

		if(branch.equals("1")) {
			if(position.equals("3") || position.equals("4")) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if(branch.equals("2") || branch.equals("3") || branch.equals("4" )) {
			if(position.equals("1" )|| position.equals( "2")) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
