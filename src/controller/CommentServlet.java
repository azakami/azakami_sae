
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

/**
 * Servlet implementation class NewMessageServlet
 */
@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");
		Comment comment = new Comment();
		comment.setText(request.getParameter("text"));

		if (request.getParameter("text").length() > 500 || StringUtils.isEmpty(request.getParameter("text")) == true) {
			messages.add("コメントは500字以内で入力してください。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		}
		comment.setMessagesId(Integer.parseInt(request.getParameter("message_id")));
		comment.setUserId(user.getId());

		new CommentService().register(comment);

		response.sendRedirect("./");

	}
}