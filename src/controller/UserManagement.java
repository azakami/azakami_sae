package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class UserManagement
 */
@WebServlet("/UserManagement")
public class UserManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserManagement() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();
		if (user == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
			return;
		} else if (user.getPosition() != 1) {
			messages.add("権限がありません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		}
		List<User> userList = new UserService().findAll();
		session.setAttribute("userList", userList);

		request.getRequestDispatcher("WEB-INF/jsp/usermanagement.jsp").forward(request, response);
	}
}
