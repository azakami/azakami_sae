package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class EditUser
 */
@WebServlet("/EditUser")
public class EditUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		if (session.getAttribute("loginUser") == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
			return;
		}

		User user = (User) session.getAttribute("loginUser");

		List<String> messages = new ArrayList<String>();

		if (user == null) {
			response.sendRedirect("login");
			return;
		} else if (user.getPosition() != 1) {
			messages.add("アクセス権限が付与されていません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		}

		User editTarget = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		session.setAttribute("editTarget", editTarget);
		request.getRequestDispatcher("WEB-INF/jsp/edituser.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		//隠し入力
		int id = Integer.parseInt(request.getParameter("id"));

		//フォームに入力された内容
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String chekePassword = request.getParameter("chekePassword");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if (login_id.length() < 6 || login_id.length() > 20 || !login_id.matches("^[0-9a-zA-Z]+$")) {
			messages.add("ログインID(半角英数字で6文字以上20文字以下)を入力してください。");
		}

		if (!(password == "" && chekePassword == "")) {
			if (password.length() < 6 || password.length() > 20) {
				messages.add("パスワード(記号を含む全ての半角文字で6文字以上20文字以下)を入力してください。");
			}

			if (!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")) {
				messages.add("パスワード(記号を含む全ての半角文字で6文字以上20文字以下)を入力してください。");
			}
			if (!password.equals(chekePassword)) {
				messages.add("登録用パスワードと確認用パスワードの値が相違しています。");
			}
		}

		if (StringUtils.isEmpty(name) == true || name.length() > 10) {
			messages.add("名前(10文字以下)を入力してください。");
		}

		if(branch ==1) {
			if(position == 3 || position == 4) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if(branch == 2 || branch == 3 || branch == 4) {
			if(position == 1 || position == 2) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}

		if (messages.size() != 0) {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("EditUser?id=" + id);
			return;
		}

		User user = new User();
		user.setId(id);
		user.setLogin_id(login_id);
		user.setPassword(password);
		user.setName(name);
		user.setBranch(branch);
		user.setPosition(position);

		new UserService().update(user);
		//ユーザー管理画面に遷移
		response.sendRedirect("UserManagement");
	}

}
