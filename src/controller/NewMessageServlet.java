package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

/**
 * Servlet implementation class NewMessageServlet
 */
@WebServlet("/NewMessage")
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewMessageServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		if (session.getAttribute("loginUser") == null) {
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
			return;
		}

		request.getRequestDispatcher("WEB-INF/jsp/NewMessage.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setMessage(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("NewMessage");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String message = request.getParameter("message");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(title) == true) {
			messages.add("件名を入力してください");
		}

		if (30 < title.length()) {
			messages.add("件名を30字以内で入力してください");
		}

		if (StringUtils.isEmpty(message) == true) {
			messages.add("本文を入力してください");
		}

		if (1000 < message.length()) {
			messages.add("本文を1000字以内で入力してください");
		}

		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリを入力してください");
		}

		if (10 < category.length()) {
			messages.add("カテゴリーを10字以内で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}