package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.GetComments;
import exception.SQLRuntimeException;

public class GetCommentsDao {

	public List<GetComments> getComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id ");
			sql.append(",messages.id as message ");
			sql.append(",comments.text ");
			sql.append(",users.login_id as comment_loginid ");
			sql.append(",users.name as comment_name ");
			sql.append(",comments.created_date ");
			sql.append("FROM comments ");
			sql.append("JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("JOIN messages ");
			sql.append("ON comments.messages_id = messages.id ");
			sql.append("ORDER BY created_date DESC " );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<GetComments> comments = toCommentsList(rs);
			return comments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<GetComments> toCommentsList(ResultSet rs)
			throws SQLException {

		List<GetComments> comments = new ArrayList<GetComments>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int message = rs.getInt("message");
				String text = rs.getString("text");
				String commentLoginId =  rs.getString("comment_loginid");
				String commentName =  rs.getString("comment_name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				GetComments comment = new GetComments();
				comment.setId(id);
				comment.setMessage(message);
				comment.setText(text);
				comment.setComment_loginid(commentLoginId);
				comment.setComment_name(commentName);
				comment.setCreated_date(createdDate);

				comments.add(comment);
			}
			return comments;
		} finally {
			close(rs);
		}
	}

}