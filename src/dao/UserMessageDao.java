package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title,");
			sql.append("messages.message as message,");
			sql.append("messages.category as category,");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id,  ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_date DESC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> selectCategory(Connection connection, UserMessage userMessage) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.message as message, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("where category like  ? ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + userMessage.getCategory() + "%");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> selectPeriod(Connection connection, UserMessage userMessage) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.message as message, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date ");
			sql.append("BETWEEN ? AND DATE_ADD( ? ,INTERVAL 1 DAY)");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setTimestamp(1, userMessage.getCreated_date());
			ps.setTimestamp(2, userMessage.getEndCreated_date());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> selectCategoryPeriod(Connection connection, UserMessage userMessage) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.title as title, ");
			sql.append("messages.message as message, ");
			sql.append("messages.category as category, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.category like  ? ");
			sql.append("AND messages.created_date BETWEEN ? AND ? ");
			sql.append("ORDER BY created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, "%" + userMessage.getCategory() + "%");
			ps.setTimestamp(2, userMessage.getCreated_date());
			ps.setTimestamp(3, userMessage.getEndCreated_date());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String message = rs.getString("message");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage userMessage = new UserMessage();
				userMessage.setLogin_id(login_id);
				userMessage.setName(name);
				userMessage.setId(id);
				userMessage.setUserId(userId);
				userMessage.setTitle(title);
				userMessage.setMessage(message);
				userMessage.setCategory(category);
				userMessage.setCreated_date(createdDate);

				ret.add(userMessage);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}