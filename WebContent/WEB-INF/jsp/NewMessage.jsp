<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<input type="button" onclick="location.href='./'" value="ホーム">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="newmessege-form">
		<form action="NewMessage" method="post">
			<input type="hidden" name="user_id" value="${loginUser.id }">
			<label for= "title">件名</label><input type="text" name="title" size="100" maxlength="50"
				placeholder="投稿の件名(30文字以下)です。空のまま登録することはできません。"> <br>
			<label for = "message">本文</label><textarea name="message" cols="100" rows="5" class="tweet-box"
				placeholder="投稿の本文(1000文字以下)です。空のまま登録することはできません。"></textarea>
			<br> <label for = "category">カテゴリー</label><input type="text" name="category" size="80" maxlength="30"
				placeholder="投稿内容のカテゴリー(10文字以下)です。空のまま登録することはできません。"> <br>
			<input type="submit" value="投稿">
		</form>
	</div>

	<div class="copylight">Copyright(c)sae</div>
</body>

</body>
</html>