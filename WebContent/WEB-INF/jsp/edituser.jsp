<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editTarget.name}の編集画面</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="/azakami_sae/EditUser" method="post">
			<input name="id" value="${editTarget.id}" id="id" type="hidden" /> <label
				for="login_id">ログインID</label> <input type=text name="login_id"
				value="${editTarget.login_id}" /> <br /> <label for="password">パスワード(変更用)</label>
			<input type="password" name="password" id="password" /> <br /> <label
				for="chekePassword">パスワード(確認用)</label> <input type="password"
				name="chekePassword" id="chekePassword" /> <br /> <label
				for="name">名称</label> <input type=text name="name"
				value="${editTarget.name}" /><br /> <label for="branch">支店</label>
			<select name="branch">
				<option value="1">本社</option>
				<option value="2">支店A</option>
				<option value="3">支店B</option>
				<option value="4">支店C</option>
			</select> <br /> <label for="position">部署・役職</label> <select name="position">
				<option value="1">総務人事担当者</option>
				<option value="2">情報管理担当者</option>
				<option value="3">支店長</option>
				<option value="4">社員</option>
			</select> <br /> <br> <input type="submit" value="編集" /> <input
				type="button" onclick="location.href='UserManagement'" value="戻る">
		</form>
		<div class="copyright">Copyright(c)sae</div>
	</div>
</body>
</html>
