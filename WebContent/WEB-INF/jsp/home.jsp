<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script>
	function disp() {
		if (window.confirm('この投稿を削除しますか？')) {
			window.alert('投稿を削除します。');
			location.href = "./";
		} else {
			window.alert('削除は中止されました。');
		}
	}
</script>
</head>
<body>
	<div class="main-contents">
		<input type="button" onclick="location.href='UserManagement'"
			value="ユーザー管理画面"> <input type="button"
			onclick="location.href='NewMessage'" value="新規投稿画面"><input
			type="button" onclick="location.href='Logout'" value="ログアウト"><br>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="./" method="get">
			<br> <label for="カテゴリー">カテゴリー <input type=text
				name="category" id="category" placeholder="カテゴリーで検索できます。" /></label> 投稿開始日<input
				type="date" name="start"> 投稿終了日<input type="date" name="end"><input
				type="submit" value="検索">
		</form>
		<c:forEach var="ret" items="${ret}">
			<div class="message">
				<div class="message-box">
					登録者:
					<c:out value="${ret.name}" />
					(
					<c:out value="${ret.login_id}" />
					) <br> 件名:
					<c:out value="${ret.title}" />
					<br> 本文:
					<c:out value="${ret.message}" />
					<br> カテゴリー:
					<c:out value="${ret.category}" />
					<br> 登録日時:
					<fmt:formatDate value="${ret.created_date}"
						pattern="yyyy/MM/dd  HH:mm" />
					<c:if test="${ret.login_id == loginUser.login_id}">
						<form action="./" method="post">
							<input type="hidden" name="message_id" value="${ret.id}" id="id">
							<input type="submit" value="削除" onClick="disp()">
						</form>
					</c:if>
				</div>
			</div>
			<div class="comment-form">
				<form action="CommentServlet" method="post">
					<input name="id" value="${loginUser.id}" id="id" type="hidden" />
					<input name="message_id" value="${ret.id}" id="id" type="hidden" />

					<label for="text">コメント</label>
					<textarea name="text" cols="80" rows="5" class="text-box"
						placeholder="500文字以下。空のまま登録することはできません。"></textarea>
					<br /> <input type="submit" value="コメント">
				</form>
			</div>
			<br>
			<div class="comments">
				<c:forEach var="comments" items="${comments}">
					<c:if test="${ret.id == comments.message}">
						<div class="comment-box">
							登録者:
							<c:out value="${comments.comment_loginid}" />
							(
							<c:out value="${comments.comment_name}" />
							) <br> 本文:
							<c:out value="${comments.text}" />
							<br> 登録日時:
							<fmt:formatDate value="${comments.created_date}"
								pattern="yyyy/MM/dd  HH:mm" />
							<c:if test="${comments.comment_loginid == loginUser.login_id}">
								<form action="./" method="post">
									<input type="hidden" name="comment_id" value="${comments.id}"
										id="id"> <input type="submit" value="削除"
										onClick="disp()">
								</form>
							</c:if>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</c:forEach>
	</div>
</body>
</html>