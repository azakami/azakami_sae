<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<form action="login" method="post">
			<br /> <label for="login_id">ログインID</label> <input type=text
				name="login_id" id="login_id" /> <br /> <label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br /> <input
				type="submit" value="ログイン" /> <br />
		</form>
		<div class="copylight">Copyright(c)sae</div>
	</div>
</body>
</html>