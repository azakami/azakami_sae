<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理画面</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script>
	function stop() {
		if (window.confirm('このアカウント機能を停止しますか？')) {
			window.confirm('アカウント機能を停止します。');
		} else {
			window.alert('このアカウントは現在使用可能です。');
			return false;
		}
	}
	function revival() {
		if (window.confirm('このアカウント機能を復活させますか？')) {
			window.confirm('アカウント機能を復活させます。');
		} else {
			window.alert('このアカウントは現在使用できません。');
			return false;
		}
	}
</script>
</head>
<body>
	<div class="main-contents">
		<input type="button" onclick="location.href='SignUp'" value="ユーザー新規登録">
		<input type="button" onclick="location.href='./'" value="ホーム">
		<table border="1">
			<tr>
				<th>ID</th>
				<th>ログインID</th>
				<th>名称</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>ユーザー</th>
				<th>登録日時</th>
				<th>更新日時</th>
				<th>編集</th>
			</tr>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td><p>
							<c:out value="${user.id}" />
						</p></td>
					<td>
						<p>
							<c:out value="${user.login_id}" />
						</p>
					</td>
					<td>
						<p>
							<c:out value="${user.name}" />
						</p>
					</td>
					<td>
						<p>
							<c:out value="${user.branchName}" />
						</p>
					</td>
					<td>
						<p>
							<c:out value="${user.positionName}" />
						</p>
					</td>
					<td><c:choose>
							<c:when test="${user.stop == 0}">
								<form method="get" action="Stop">
									<input type="hidden" name="id" value="${user.id}"> <input
										type="submit" onClick="return stop()" value="停止する">
								</form>
							</c:when>
							<c:otherwise>
								<form method="get" action="Stop">
									<input type="hidden" name="id" value="${user.id}"> <input
										type="submit" onClick="return revival()" value="復活する">
								</form>
							</c:otherwise>
						</c:choose></td>
					<td>
						<p>
							<fmt:formatDate value="${user.created_date}"
								pattern="yyyy/MM/dd  HH:mm" />
					<td>
						<p>
							<fmt:formatDate value="${user.updated_date}"
								pattern="yyyy/MM/dd  HH:mm" />
						</p>
					</td>
					<td>
						<form action="/azakami_sae/EditUser" method="get">
							<input name="id" value="${user.id}" type="hidden"> <input
								type="submit" value="編集" />
						</form>
					</td>
				</tr>
			</c:forEach>
		</table>
		<div class="copyright">Copyright(c)sae</div>
	</div>
</body>
</html>